<?php

define('DBHOST', '10.16.1.12');
define('DBUSER', 'root');
define('DBPASS', 'ZXasqw12');
define('DBNAME', 'task2');
define('TBNAME', 'users');

class DB {
    protected $connection;

    public function __construct() {
        if (!isset($this->connection)) {
            $this->connect();
        }
    }

    private function connect() {
        $this->connection = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    }

    public function getConnection() {
        return $this->connection;
    }
}

class UserRegistration {
    private $conn;

    public function __construct() {
        $db = new DB();
        $this->conn = $db->getConnection();
    }

    private function checkUserExists($username) {
        if ($query = $this->conn->prepare("SELECT * FROM `users` WHERE `username` = ?")) {
            $query->bind_param("s", $username);
            $query->execute();
            $query->store_result();

        }
        $rows = $query->num_rows;
        if ($rows > 0) return true;
        return false;
    }

    public function registerNewUser($username, $password) {
        if ($this->checkUserExists($username)) return false;

        if ($stmt = $this->conn->prepare("INSERT INTO `users` (`username`, `userpwd`) VALUES (?, ?)")) {
            $password_hashed = password_hash($password, PASSWORD_DEFAULT);
            $stmt->bind_param('ss', $username, $password_hashed);
            $stmt->execute();
            return true;
        }
    }
}

// Registration
$registration = new UserRegistration;
if ($registration->registerNewUser($_POST['username'], $_POST['password'])) {
    header('location:./thanks.php');
} else {
    header('location:./?error=existed');
}
