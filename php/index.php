<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
        <link rel="stylesheet" href="./assets/css/style.css">
        <title>Register</title>
    </head>
    <body>
        <div id="login">
        <div class="login-card">
            <div class="card-title">
                <h1>Register</h1>
            </div>
            <div class="content">
                <form method="POST" action="registration.php">
                    <input id="username" type="text" name="username" title="username" placeholder="Username" required autofocus>
                    <input id="password" type="password" name="password" title="password" placeholder="********" required>
                    <button type="submit" class="btn btn-primary" id="register" name="registerButton">Register</button>
                </form>
            </div>
        </div>
        </div>
    </body>
</html>
